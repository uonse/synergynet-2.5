package apps.mtdesktop.desktop.tree.nodes;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;

public class TopNode extends DefaultMutableTreeNode{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -6987549845971451228L;

	public TopNode (){
		  super("Table");
	}

	public Icon getIcon(){
		return null;
	}
}
