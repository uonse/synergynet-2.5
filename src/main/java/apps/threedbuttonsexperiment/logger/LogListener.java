package apps.threedbuttonsexperiment.logger;

public interface LogListener {
	public void keyPressed(String key);
	public void deleteKeyPressed();
	public void submitKeyPressed();
}
