package apps.threedbuttonsexperiment.calculator.component;

public interface TaskListener {
	public void taskCompleted();
}
