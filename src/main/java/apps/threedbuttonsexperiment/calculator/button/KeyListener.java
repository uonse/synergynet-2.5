package apps.threedbuttonsexperiment.calculator.button;

public interface KeyListener {
	public void keyPressed(String key);
}
