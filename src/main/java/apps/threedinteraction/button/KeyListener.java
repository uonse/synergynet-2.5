package apps.threedinteraction.button;

public interface KeyListener {
	public void keyPressed(String key);
}
