package synergynetframework.appsystem.services.net.rapidnetworkmanager.handlers;

public interface MessageProcessor {
	public void process(Object obj);
}
