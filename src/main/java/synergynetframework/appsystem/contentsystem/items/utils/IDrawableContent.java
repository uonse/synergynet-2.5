package synergynetframework.appsystem.contentsystem.items.utils;

public interface IDrawableContent {

	public void drawContent(Object drawingHandle);
}
