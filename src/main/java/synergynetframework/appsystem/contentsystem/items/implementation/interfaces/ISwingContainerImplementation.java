package synergynetframework.appsystem.contentsystem.items.implementation.interfaces;

import javax.swing.JDesktopPane;

public interface ISwingContainerImplementation {
	JDesktopPane getJDesktopPane();
}
